export function setLanguage( name ) {
    return dispatch => {
        fetch('/languages/index.json')
        .then( res => {
            if ( !res.ok ) {
                throw(res)
            } else {
                return res.json();
            }
        })
        .then( data => {
            for(let i in data) {
                if( data[i] === name ) {
                    fetch('/languages/' + name + '.json')
                    .then( res => {
                        if ( !res.ok ) {
                            throw(res);
                        } else {
                            return res.json();
                        }
                    })
                    .then( textSet => {
                        dispatch({
                            type : "SET_LANGUAGE",
                            payload : textSet
                        })
                    });
                    break;
                }
            }
        })
        .catch(err => {
            err.json().then( msg => {
                console.log(msg);
                alert(msg.message);
            });
        })


    } 
}