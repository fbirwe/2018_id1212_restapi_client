import React, { Component } from 'react';
import "./Dropdown.scss";

class Dropdown extends Component {

    constructor(props) {
        super(props);

        this.state = {
            visible : false,
            active : null
        }
    }

    static getDerivedStateFromProps(props, state) {
        for(let i in props) {
            if(state[i] !== props[i]) {
                return props;
            }
        }

        return state;
    }

    selectItem(i, ev) {
        if( this.props.onUpdate !== null && typeof this.props.onUpdate === "function" ) {
            this.props.onUpdate(i);
        }

        this.setState({
            visible : false,
            active : i
        })
    }

    toggleList(ev) {
        const { visible } = this.state;

        this.setState({
            visible : !visible
        })
    }

    render() {
        const wrapperStyle = {
            width : this.props.width || '100%'
        }

        const listStyle = {
            // width : this.props.width || '100%',
            display : this.state.visible ? "block" : "none"
        }

        let activeElement = null;

        if( this.props.active !== null && this.props.active !== -1 ) {
            activeElement = this.props.children[this.props.active];
        }

        return (
            <div className="dropdown" style={ wrapperStyle }>
                <div className="preview" onClick={ this.toggleList.bind(this) }>
                    { activeElement }
                </div>
                <div className="list" style={ listStyle }>
                    { this.props.children.map((el, i) => {
                        return (
                            <div className="item" onClick={ this.selectItem.bind(this, i)}>
                                { el }
                            </div>
                        )
                    }) }
                </div>
            </div>
        )
    }
}

export default Dropdown