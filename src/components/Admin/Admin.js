import React, { Component } from 'react';
import '../../App.scss'
import './Admin.scss';
import LanguageManagement from '../LanguageManagement/LanguageManagement.js';
import AudioManagement from '../AudioManagement/AudioManagement.js';
import ChooseLanguage from '../ChooseLanguage/ChooseLanguage';
import { connect } from 'react-redux';


class Admin extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activeTab : 0
        }
    }
    
    changeTab(i, ev) {
        this.setState({
            activeTab : i
        })
    }

    render() {
        let activeTab = null;
        switch ( this.state.activeTab ) {
            case 0 :
                activeTab = <LanguageManagement></LanguageManagement>;
                break;
            case 1 :
                activeTab = <AudioManagement></AudioManagement>
                break;
            default :
                activeTab = <LanguageManagement></LanguageManagement>
        }
        
        const tabs = [
            {
                text : this.props.TEXTS !== undefined ? this.props.TEXTS.admin.nav.language : "language",
            },
            {
                text : this.props.TEXTS !== undefined ? this.props.TEXTS.admin.nav.audio : "audio"
            }
        ]

        return (
            <div className="App">
                <h1>{ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.headline : "admin interface" }</h1>
                <div>
                    <ChooseLanguage></ChooseLanguage>
                </div>
                <ul className="nav">
                    { tabs.map((el, i) => {
                        let classes = [];
                        if( this.state.activeTab === i ) {
                            classes.push( "active" );
                        }

                        return <li key={"nav_" + i} className={ classes.join(" ") } onClick={ this.changeTab.bind(this, i)}>{ el.text }</li>
                    })}
                </ul>
                <div className="content_wrapper">
                    { activeTab }
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
      TEXTS : state,
    }
}  

export default connect(mapStateToProps)(Admin);
