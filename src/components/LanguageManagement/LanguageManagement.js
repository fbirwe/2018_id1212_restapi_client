import React, { Component } from 'react';
import { connect } from 'react-redux';
import { DOMAIN } from '../../constants'; 
import '../../App.scss'
import EditLanguageOverlay from './EditLanguageOverlay';
import LanguagePreview from '../LanguagePreview/LanguagePreview';
import "./LanguageManagement.scss";

class LanguageManagement extends Component {

    constructor() {
        super();

        this.state = {
            language : {
                shortcut : "",
                name : "",
                file : null
            },
            toChangeLanguage : -1,
            languages : [],
            flags : [],
            modal : {
                editLanguage : false,
                createLanguage : false
            }
        };
    }

    componentDidMount() {
        this.init();
    }

    componentDidUpdate() {
        //this.init();
    }

    init() {
        fetch( DOMAIN + "/language")
        .then( res => {
        
            if ( !res.ok ) {
                throw(res);
            } else {
                return res.json();
            }
        })
        .then( data => {
            this.setState({
                languages : data
            })
        })
        .catch(err => {
            err.json().then( msg => {
                console.log(msg);
                alert(msg.message);
            });
        })
    }

    changeLanguage( type, ev ) {
        const { language } = this.state;

        language[type] = ev.target.value;

        this.setState({
            language
        });
    }

    createLanguage() {
        const { language } = this.state;

        const formData = new FormData();

        if( language.file !== undefined ) {
            formData.append("file", language.file)
        }   

        fetch(`${ DOMAIN }/language/${language.shortcut}/name/${language.name}`, {
            method : "POST",
            body : formData
        })
        .then(res => {
            if( !res.ok ) {
                throw(res);
            } else {
                return res.json();
            }
        })
        .then(data => {
            language.shortcut = "";
            language.name = "";

            this.setState({
                language
            });

            this.init();
        })
        .catch(err => {
            err.json().then( msg => {
                console.log(msg);
                alert(msg.message);
            });
        })
    }

    deleteLanguage( index, ev ) {
        fetch(`${ DOMAIN }/language/${this.state.languages[index].shortcut}`, {
            method : 'DELETE'
        })
        .then(res => {
            if( !res.ok ) {
                throw res;
            } else {
                this.init();
            }
        })
        .catch(err => {
            err.json().then( msg => {
                console.log(msg);
                alert(msg.message);
            });
        })
    }

    editLanguage( index, ev ) {
        const { modal } = this.state;

        modal.editLanguage = true;

        this.setState({
            modal,
            toChangeLanguage : index
        });
    }

    onChange_updateLanguage( language ) {
        const { languages } = this.state;
        languages[this.state.toChangeLanguage] = language;

        this.setState({
            languages
        })

    }

    onChange_createLanguage( language ) {
        this.setState({
            language
        });
    }

    updateLanguage() {
        const language = this.state.languages[this.state.toChangeLanguage];

        const formData = new FormData();

        if( language.file !== undefined ) {
            formData.append("file", language.file)
        }    

        fetch(`${ DOMAIN }/language/${language.shortcut}/name/${language.name}`, {
            method : "PUT",
            body : formData
        })
        .then(res => {
            if( !res.ok ) {
                throw( res );
            } else {
                this.init()
            }
        })
        .catch(err => {
            err.json().then( msg => {
                console.log(msg);
                alert(msg.message);
            });
        })
    }

    hideModal( modalUI, ev ) {
        const { modal } = this.state;

        modal[modalUI] = false;

        this.setState({
            modal
        });
    }

    showModal( modalUI, ev ) {
        const { modal } = this.state;

        modal[modalUI] = true;

        this.setState({
            modal
        });
    }


    render() {       
        return (
            <div>
                <h1>{ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.language_management.headline : "manage languages" }</h1>
                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th>{ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.language_table.th_shortcut : "shortcut" }</th>
                            <th>{ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.language_table.th_name : "name" }</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.state.languages.map( (el, i) => (
                            <tr key={"language_row_" + i}>
                                <td>
                                    <LanguagePreview shortcut={ el.shortcut } size={30}></LanguagePreview>
                                </td>
                                <td>{el.shortcut}</td>
                                <td>{el.name}</td>
                                <td>
                                    <button className="ui button" onClick={ this.editLanguage.bind(this, i)}><i className="edit icon"></i></button>
                                    <button className="ui button red" onClick={ this.deleteLanguage.bind(this, i)}><i className="trash icon"></i></button>
                                </td>                            
                            </tr>
                        ))}
                    </tbody>
                </table>
                <button className="ui button" onClick={this.showModal.bind(this, "createLanguage")}>{ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.language_table.create_language : "create language" }</button>
                <EditLanguageOverlay 
                    hide={ this.hideModal.bind(this, 'editLanguage') } 
                    onOK={ this.updateLanguage.bind(this) }
                    active={ this.state.modal.editLanguage } 
                    type="edit" 
                    language={ this.state.toChangeLanguage !== -1 ? this.state.languages[this.state.toChangeLanguage] : {} }
                    onChange={ this.onChange_updateLanguage.bind(this) }>
                </EditLanguageOverlay>
                <EditLanguageOverlay 
                    hide={ this.hideModal.bind(this, 'createLanguage') } 
                    onOK={ this.createLanguage.bind(this) }
                    active={ this.state.modal.createLanguage } 
                    type="create" 
                    language={ this.state.language }
                    onChange={ this.onChange_createLanguage.bind(this) }>
                </EditLanguageOverlay>

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
      TEXTS : state,
    }
}  

export default connect(mapStateToProps)(LanguageManagement);