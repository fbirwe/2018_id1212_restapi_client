import React, { Component } from 'react';
import { connect } from 'react-redux';
import Overlay from '../Overlay/Overlay';
import LanguagePreview from '../LanguagePreview/LanguagePreview';
import './EditLanguageOverlay.scss';

class EditLanguageOverlay extends Component {

    changeLanguage( value, ev ) {
        const newLanguage = JSON.parse(JSON.stringify( this.props.language ));
        
        if( value !== "file" ) {
            newLanguage[value] = ev.target.value;
        } else {
            newLanguage[value] = ev.target.files[0];
        }

        if( this.props.onChange !== undefined && typeof this.props.onChange === 'function' ) {
            this.props.onChange( newLanguage );
        }
    }

    render() {

        let headline = null;
        if( this.props.type === "edit" ) {
            headline = (
                <div className="headline">
                    <LanguagePreview shortcut={ this.props.language.shortcut } size={50}></LanguagePreview>
                    <h2>
                        { this.props.language.shortcut } - { this.props.TEXTS !== undefined ? this.props.TEXTS.admin.language_management.editModal.headlineEdit :  "edit language"}
                    </h2>
                </div>

            )
        }

        let shortcutInput = null;
        if( this.props.type === "create" ) {
            headline = (
                <h2>{ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.language_management.editModal.headlineCreate : "create a new language" }</h2>
            );
            shortcutInput = (
                <div className="ui input">
                    <input className="ui input" type="text" placeholder={ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.language_table.th_shortcut : "shortcut" } value={this.props.language.shortcut} onChange={this.changeLanguage.bind(this, "shortcut")}></input>                        
                </div>
            )
        }

        return (
            <Overlay active={this.props.active} 
                hide={ this.props.hide } 
                onOK={ this.props.onOK }
                onCancel={() => {} }
            >
                { headline }
                { shortcutInput }
                <div className="ui input">
                    <input type="text" placeholder={ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.language_table.th_name : "name" } value={ this.props.language.name } onChange={ this.changeLanguage.bind(this, "name") }></input>
                </div>
        
                <div className="ui input">
                    <input type="file" onChange={this.changeLanguage.bind(this, "file")}></input>
                </div>
            </Overlay>
        )

    }
}

const mapStateToProps = state => {
    return {
      TEXTS : state,
    }
}  

export default connect(mapStateToProps)(EditLanguageOverlay);