import React from "react";
import { Route, Switch } from "react-router-dom";
import Client from '../Client/Client';
import Admin from '../Admin/Admin';

const AppRouter = () => (
  <Switch>
      <Route path="/" exact component={Client} />
      <Route path="/admin/" component={Admin} />
  </Switch>
);

export default AppRouter;