import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Overlay.scss';

class Overlay extends Component {

    hide(ev) {
        if(this.props.hide !== undefined && typeof this.props.hide === "function") {
            this.props.hide();
        }
    }

    onOK(ev) {
        this.hide();

        if(this.props.onOK !== undefined && typeof this.props.onOK === "function") {
            this.props.onOK();
        }
    }

    onCancel(ev) {
        this.hide();

        if(this.props.onCancel !== undefined && typeof this.props.onCancel === "function") {
            this.props.onCancel();
        }
    }

    render() {
        const size = {
            width : window.innerWidth + "px",
            height : window.innerHeight + "px"
        }

        // OK Button
        let okButton = null;
        if( this.props.onOK !== null && typeof this.props.onOK === 'function' ) {
            okButton = <button className="ui button" onClick={ this.onOK.bind(this) }>{ this.props.TEXTS ? this.props.TEXTS.overlay.ok : "OK" }</button>
        }

        // Cancel Button
        let cancelButton = null;
        if( this.props.onCancel !== null && typeof this.props.onCancel === 'function' ) {
            cancelButton = <button className="ui button" onClick={ this.onCancel.bind(this) }>{ this.props.TEXTS ? this.props.TEXTS.overlay.cancel : "Cancel" }</button>
        }

        if( !this.props.active ) {
            return null;
        } else {
            return (
                <div className="overlay_wrapper" style={ size } onClick={ this.hide.bind(this) }>
                    <div className="overlay" onClick={(ev) => { ev.stopPropagation() } }>
                        <div className="content_wrapper">
                            { this.props.children }
                        </div>
                        <div className="button_wrapper">
                            { okButton }
                            { cancelButton }
                        </div>
                    </div>
                </div>
            )
        }
    }
}

const mapStateToProps = state => {
    return {
      TEXTS : state,
    }
}  

export default connect(mapStateToProps)(Overlay);