import React, { Component } from 'react';
import './LanguagePreview.scss';
import { DOMAIN } from '../../constants';

class LanguagePreview extends Component {
    render() {
        const style = {
            "backgroundSize" : "cover",
            "backgroundImage" : `url(${ DOMAIN }/language/${ this.props.shortcut }/image)`,
            "width" : this.props.size + "px" || "50px",
            "height" : this.props.size + "px" || "50px"
        };

        return (
            <div className="language_preview" style={ style }></div>
        )
    }
}

export default LanguagePreview;