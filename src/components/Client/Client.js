import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../../App.scss';
import { DOMAIN } from '../../constants';
import Overlay from '../Overlay/Overlay';
import Dropdown from '../Dropdown/Dropdown';
import LanguagePreview from '../LanguagePreview/LanguagePreview';
import ChooseLanguage from '../ChooseLanguage/ChooseLanguage';
import { setLanguage } from '../../actions/languageActions';


class Client extends Component {
  constructor( props ) {
    super( props );

    this.state = {
      languages : [],
      selectedLanguage : -1,
      exhibits : [],
      modal : {
        languageProposal : true
      }
    }
  }

  componentDidMount() {
    // get languages
    fetch(`${ DOMAIN }/language`)
    .then(res => {
      if( !res.ok ) {
        throw(res);
      } else {
        return res.json();
      }
    })
    .then(languages => {
      this.setState({
        languages
      })
    })
    .catch(err => {
      err.json().then( msg => {
          console.log(msg);
          alert(msg.message);
      });
  })
  }

  hideModal( modalName ) {
    const { modal } = this.state;

    modal[modalName] = false;

    this.setState({
      modal
    })
  }

  showModal( modalName ) {
    const { modal } = this.state;

    modal[modalName] = true;

    this.setState({
      modal
    })
  }

  fetchExhibits( language ){
    fetch(`${ DOMAIN }/exhibit/language/${ language.shortcut }`)
    .then( res => {
      if( !res.ok ) {
        throw(res);
      } else {
        return res.json();
      }
    })
    .then(exhibits => {
      this.setState({
        exhibits
      })
    })
    .catch(err => {
      err.json().then( msg => {
          console.log(msg);
          alert(msg.message);
      });
  })
  }

  changeLanguage( index ) {
    this.fetchExhibits( this.state.languages[index] );

    this.props.setLanguage( this.state.languages[index].shortcut );

    this.setState({
      selectedLanguage : index
    })
  }

  render() {
    // find language
    let languageProposal = null;
    if( this.state.selectedLanguage === -1 ) {
      for(let i in this.state.languages) {
          if(navigator.language.includes( this.state.languages[i].shortcut ) && parseInt(i) !== this.state.selectedLanguage ) {
            languageProposal = (
              <Overlay active={ this.state.modal.languageProposal } hide={ this.hideModal.bind(this, "languageProposal") }onOK={ this.changeLanguage.bind(this, i) } onCancel={ () => {} }>
                <h2>{ this.props.TEXTS !== undefined ? this.props.TEXTS.client.language_proposal.title : "We found your language"}</h2>
                <p>{ this.props.TEXTS !== undefined ? this.props.TEXTS.client.language_proposal.text : "Do you want to take the audioguide in: "}{this.state.languages[i].name !== null ? this.state.languages[i].name : this.state.languages[i].shortcut}?</p>
              </Overlay>
            )
            break;
          }
      }
    }

    let exhibitList = null;
    if( this.state.selectedLanguage !== -1 ) {

      if( this.state.exhibits.length > 0 ) {
        exhibitList = (
          <div className="exhibit_wrapper">
          { this.state.exhibits.map(el => {
            const elements = [];

            if( el.description !== null ) {
              elements.push( <p>{ el.description }</p> );
            }

            if( el.audioDir !== null ) {
              elements.push( 
                <audio controls>
                  <source src={ `${ DOMAIN }/exhibit/station/${ el.exhibitStation.id }/language/${ el.language.shortcut }/audio` } type="audio/mpeg"></source>
                </audio> 
              );
            }

            return (
              <div className="exhibit">
                <h2>{el.exhibitStation.id}{ el.name !== null ? " – " + el.name : "" }</h2>
                { elements.map(el => el)}
              </div>
            )
          })}
        </div>
      )
    } else {
      exhibitList = <p>{ this.props.TEXTS !== undefined ? this.props.TEXTS.client.noExhibitsAvailable : "no exhibits available in this language"}</p>
    }

      
  }

    return (
      <div className="App">
        <h1>{ this.props.TEXTS !== undefined ? this.props.TEXTS.client.welcome : "Welcome at vasamuseum!"}</h1>
        <div>
            <ChooseLanguage></ChooseLanguage>
        </div>
        <div className="content_wrapper">
          <p>{ this.props.TEXTS !== undefined ? this.props.TEXTS.client.choose_language : "Please choose your favourite language" }</p>
          <Dropdown active={ this.state.selectedLanguage }onUpdate={ this.changeLanguage.bind(this) }>
            {
              this.state.languages.map(el => {
                const elList = [];

                if( el.flag !== null ) {
                  elList.push( <LanguagePreview size={30} shortcut={el.shortcut}></LanguagePreview>)
                }

                const pStyle= {
                  marginLeft : '10px',
                  marginTop : '5px'
                }

                if( el.name !== null ) {
                  elList.push( <p style={pStyle}>{ el.name }</p> );
                } else {
                  elList.push( <p style={pStyle}>{ el.shortcut }</p> );
                }

                return (
                  <div>
                    { elList.map( el2 => el2 )}
                  </div>
                );
              })
          }
          </Dropdown>
          { languageProposal }
          { exhibitList }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    TEXTS : state,
  }
}  

const mapDispatchToProps = dispatch => {
  return {
      setLanguage : (name) => {
          dispatch(setLanguage(name))
      }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Client);