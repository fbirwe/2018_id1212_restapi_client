import React, { Component } from 'react';
import { DOMAIN } from '../../constants';
import LanguagePreview from '../LanguagePreview/LanguagePreview';
import "./AudioManagement.scss";
import EditAudioOverlay from './EditAudioOverlay';
import { connect } from 'react-redux';


class AudioManagement extends Component {
    constructor( props ) {
        super( props );
    
        this.state = {
            exhibits : [],
            modal : {
                createExhibit : false,
                editExhibit : false
            },
            toEditExhibit : [],
            newExhibit : {
                exhibitStation : {
                    id : 0
                },
                description : "",
                language : {
                    shortcut : "",
                    name : ""
                },
                audioFile : null,
                name : ""
            }
        }
    }

    componentDidMount() {
        this.init();
    }

    addExhibit(ev) {
        const formData = new FormData();

        formData.append( 'name', this.state.newExhibit.name );
        formData.append( 'file', this.state.newExhibit.audioFile );
        formData.append( 'description', this.state.newExhibit.description );

        fetch( `${DOMAIN}/exhibit/station/${ this.state.newExhibit.exhibitStation.id }/language/${ this.state.newExhibit.language.shortcut }`, {
            method : "POST",
            body : formData
        })
        .then(res => {
            if( !res.ok ) {
                throw res;
            } else {
                this.init();
            }
        })
        .catch(err => {
            err.json().then( msg => {
                console.log(msg);
                alert(msg.message);
            });
        })

    }

    editExhibit( i, j, ev ) {
        this.setState({
            toEditExhibit : [i,j]
        })

        this.showModal("editExhibit", null);
    }

    updateExhibit() {
        if( this.state.toEditExhibit.length > 0 ) {
            const exhibit = this.state.exhibits[ this.state.toEditExhibit[0] ][ this.state.toEditExhibit[1] ];

            const formData = new FormData();

            formData.append( 'name', exhibit.name );
            formData.append( 'file', exhibit.audioFile );
            formData.append( 'description', exhibit.description );
    
            fetch( `${DOMAIN}/exhibit/station/${ exhibit.exhibitStation.id }/language/${ exhibit.language.shortcut }`, {
                method : "PUT",
                body : formData
            })
            .then(res => {
                if( !res.ok ) {
                    throw res;
                } else {
                    this.hideModal("editExhibit");

                    this.setState({
                        toEditExhibit : []
                    })

                    this.init();
                }
            })
            .catch(err => {
                err.json().then( msg => {
                    console.log(msg);
                    alert(msg.message);
                });
            })
        }       
    }

    deleteExhibit( exhibit ) {
        fetch(`${ DOMAIN }/exhibit/station/${ exhibit.exhibitStation.id }/language/${ exhibit.language.shortcut }`, {
            method : "DELETE",
        })
        .then(res => {
            if( !res.ok ) {
                throw(res);
            } else {
                this.init();
            }
        })
        .catch(err => {
            err.json().then( msg => {
                console.log(msg);
                alert(msg.message);
            });
        })
    }

    hideModal( modalUI, ev ) {
        const { modal } = this.state;

        modal[modalUI] = false;

        this.setState({
            modal
        });
    }

    showModal( modalUI, ev ) {
        const { modal } = this.state;

        modal[modalUI] = true;

        this.setState({
            modal
        });
    }

    init() {
        fetch( `${DOMAIN}/exhibit/` )
        .then( res => {
            if( !res.ok ) {
                throw (res);
            } else {
                return res.json();
            }
        })
        .then( rawExhibits => {

            const exhibits = [];

            for(let i in rawExhibits) {

                if( exhibits[ rawExhibits[i].exhibitStation.id ] === undefined ) {
                    exhibits[ rawExhibits[i].exhibitStation.id ] = [];
                }
                
                exhibits[ rawExhibits[i].exhibitStation.id ].push( rawExhibits[i] );
            }

            this.setState({
                exhibits
            })
        })
        .catch(err => {
            err.json().then( msg => {
                console.log(msg);
                alert(msg.message);
            });
        })
    }

    showAccordion(i, ev) {
        if( ev.target.classList.contains("active") ) {
            document.querySelectorAll('.title').forEach(el => {
                el.classList.remove("active");
            })

            document.querySelectorAll('.content').forEach(el => {
                el.classList.remove("active");
            })
        }

        document.querySelector("#exhibit_" + i + " .title").classList.toggle("active");
        document.querySelector("#exhibit_" + i + " .content").classList.toggle("active");
    }

    updateNewExhibit( exhibit ) {
        this.setState({
            newExhibit : exhibit
        })
    }

    updateOldExhibit( exhibit ) {
        if( this.state.toEditExhibit.length > 0 ) {
            const { exhibits } = this.state;
            exhibits[this.state.toEditExhibit[0]][this.state.toEditExhibit[1]] = exhibit;

            this.setState({
                exhibits
            });
        }
    }

    render() {
        return (
            <div>
                <h1>{this.props.TEXTS !== undefined ? this.props.TEXTS.admin.audio_management.headline : "manage audio sets" }</h1>
                <div>
                    <button className="ui button" onClick={ this.showModal.bind(this, "createExhibit") }>{ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.audio_management.button_add_exhibit : "add exhibit" }</button>
                    <EditAudioOverlay hide={this.hideModal.bind(this, "createExhibit")} active={ this.state.modal.createExhibit  } onOK={ this.addExhibit.bind(this) } exhibit={ this.state.newExhibit } onChange={ this.updateNewExhibit.bind(this) } type="create"></EditAudioOverlay>
                    <EditAudioOverlay hide={this.hideModal.bind(this, "editExhibit")} active={ this.state.modal.editExhibit  } onOK={ this.updateExhibit.bind(this) } exhibit={ this.state.toEditExhibit.length > 0 ? this.state.exhibits[this.state.toEditExhibit[0]][this.state.toEditExhibit[1]] : this.state.newExhibit } onChange={ this.updateOldExhibit.bind(this) } type="edit"></EditAudioOverlay>
                </div>
                <div className="ui styled fluid accordion">
                    { this.state.exhibits.map( (el, i) => {
                        return (
                            <div id={"exhibit_" + i } key={"exhibit_" + i }>
                                <div className="title" onClick={ this.showAccordion.bind(this, i) }>
                                    <i className="dropdown icon"></i>
                                    {this.props.TEXTS !== undefined ? this.props.TEXTS.admin.audio_management.exhibit_headline : "exhibit"} {i}
                                </div>
                                <div className="content">
                                    { el.map( (el2, j) => {
                                        let languagePrev;
                                        if( el2.language.filetype !== null ) {
                                            languagePrev = (
                                                <div style={ { display : 'inline-block', verticalAlign : 'baseline' } }>
                                                    <LanguagePreview size={36} shortcut={el2.language.shortcut}></LanguagePreview>
                                                </div>
                                            );
                                        } else {
                                            languagePrev = <span className="languagePreview">{ el2.language.shortcut }</span>
                                        }

                                        let audio = null;
                                        if( el2.filetype !== null ) {
                                            audio = (
                                                <audio controls>
                                                    <source src={ `${DOMAIN}/exhibit/station/${el2.exhibitStation.id}/language/${el2.language.shortcut}/audio` } type="audio/mpeg"></source>
                                                </audio>
                                            )
                                        }

                                        return (
                                            <div className="exhibit">
                                                <h2>{el2.name}</h2>
                                                <div className="info">
                                                    { languagePrev }
                                                    <button className="ui button" onClick={ this.editExhibit.bind(this, i, j)}><i className="edit icon"></i></button>
                                                    <button className="ui button red" onClick={ this.deleteExhibit.bind(this, el2)}><i className="trash icon"></i></button>
                                                </div>
                                                { audio }
                                                <div>
                                                    { el2.description }
                                                </div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        )
                    }
                    )}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
      TEXTS : state,
    }
}  

export default connect(mapStateToProps)(AudioManagement);