import React, { Component } from 'react';
import { connect } from 'react-redux';
import Overlay from '../Overlay/Overlay';
import './EditAudioOverlay.scss';

class EditAudioOverlay extends Component {

    newExhibitChangeValue(value, ev) {
        const newExhibit = {}

        for( let i in this.props.exhibit ) {
            newExhibit[i] = this.props.exhibit[i];
        }

        switch ( value ) {
            case "description" :
                newExhibit[value] = ev.target.value;
                break;
            case "id" :
                newExhibit.exhibitStation.id = parseInt(ev.target.value);
                break;
            case "name" :
                newExhibit[value] = ev.target.value;
                break;
            case "language" :
                newExhibit.language.shortcut = ev.target.value;
                break;
            case "audioFile" :
                newExhibit.audioFile = ev.target.files[0]
                break;
            default :
        }

        if( this.props.onChange !== null && typeof this.props.onChange === 'function' ) {
            this.props.onChange(newExhibit);
        }
    }

    render() {
        let headline = null;

        if( this.props.type === "edit" ) {
            headline = (
                <h2>{ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.audio_management.editModal.headlineEdit : "edit the following exhibit"} | { this.props.exhibit.exhibitStation.id }/{ this.props.exhibit.language.shortcut }</h2>
            )
        } else if( this.props.type === "create" ) {
            headline = (
                <h2>{ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.audio_management.editModal.headlineCreate : "create a new exhibit"}</h2>
            )
        }

        return (
            <Overlay hide={ this.props.hide } active={ this.props.active  } onOK={ this.props.onOK } onCancel={() => {}}>
                { headline }
                <div className="ui input">
                    <input type="number" placeholder={ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.audio_management.placeholder_exhibit_id : "exhibit id" } value={ this.props.exhibit.exhibitStation.id } onChange={ this.newExhibitChangeValue.bind(this, "id")} ></input>
                </div>
                <div className="ui input">
                    <input type="text" placeholder={ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.audio_management.placeholder_name : "name" } value={this.props.exhibit.name} onChange={ this.newExhibitChangeValue.bind(this, "name")} ></input>
                </div>
                <div className="ui input">
                    <input type="text" placeholder={ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.audio_management.placeholder_language : "language" } value={this.props.exhibit.language.shortcut} onChange={ this.newExhibitChangeValue.bind(this, "language")} ></input>
                </div>
                <div className="ui input">
                    <input type="file" placeholder={ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.audio_management.placeholder_name : "name" } onChange={ this.newExhibitChangeValue.bind(this, "audioFile")} ></input>
                </div>
                <div>
                    <textarea placeholder={ this.props.TEXTS !== undefined ? this.props.TEXTS.admin.audio_management.placeholder_description : "description" } onChange={ this.newExhibitChangeValue.bind(this, "description")} value={ this.props.exhibit.description }>

                    </textarea>
                </div>
            </Overlay>
        )
    }
}

const mapStateToProps = state => {
    return {
      TEXTS : state,
    }
}  

export default connect(mapStateToProps)(EditAudioOverlay);