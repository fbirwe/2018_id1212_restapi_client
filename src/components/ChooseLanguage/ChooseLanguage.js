import React, { Component } from 'react';
import "./ChooseLanguage.scss";
import { connect } from 'react-redux';
import { setLanguage } from '../../actions/languageActions';

class ChooseLanguage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            availableLanguages : [],
        }
    }

    componentDidMount() {
        fetch("/languages/index.json")
        .then(res => {
            if( !res.ok ) {
                throw( res );
            } else {
                return res.json();
            }
        })
        .then(data => {
            this.setState({
                availableLanguages : data
            });
        })
        .catch(err => {
            err.json().then( msg => {
                console.log(msg);
                alert(msg.message);
            });
        })
    }

    selectLanguage( i, ev ) {
        this.props.setLanguage( this.state.availableLanguages[i] );
    }

    render() {
        let active = 0;
        for( let i in this.state.availableLanguages ) {
            if( this.props.language !== undefined && this.state.availableLanguages[i] === this.props.language.shortcut ) {
                active = parseInt(i);
            }
        }

        return(
            <ul className="chooseLanguage">
                { this.state.availableLanguages.map( (el, i) => {
                    const classString = ["languagePreview"];

                    if( i === active ) {
                        classString.push( "active" );
                    }

                    return (
                        <li onClick={ this.selectLanguage.bind(this, i) }>
                            <img className={ classString.join(" ") } src={"/languages/" + el + ".svg"} alt={el}></img>
                        </li>
                    )
                }) }
            </ul>
        )
    }
}

const mapStateToProps = state => {
    return {
      language : state,
    }
}  

const mapDispatchToProps = dispatch => {
    return {
        setLanguage : (name) => {
            dispatch(setLanguage(name))
        }
    }
  }
  

// export default ChooseLanguage;
export default connect(mapStateToProps, mapDispatchToProps)(ChooseLanguage)