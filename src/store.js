import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import logger from 'redux-logger'
import languageReducer from './reducers/languageReducer';

export default createStore(languageReducer,
{},
applyMiddleware(
    logger,
    thunk,
    promise()
));