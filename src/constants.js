import TEXTS from './languages/en.json'

// const DOMAIN = "http://localhost:8082";
const DOMAIN = "http://192.168.158.25:8082";

export {
    DOMAIN,
    TEXTS
}